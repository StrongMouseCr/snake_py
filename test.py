import pygame as pg
from random import randint
import sys

pg.init()

width = 800
height = 800

row = 20
column = 20

stepX = width / column
stepY = height / row

clock = pg.time.Clock()

display = pg.display.set_mode((width, height))
pg.display.set_caption("Sim")
textEndGame = pg.font.SysFont('arial', 40)

snake = [[10, 10, 0], [10, 11, 0]]
food = [randint(1, column), randint(1, row)]

file = open('recordsTable.txt', 'a+')

speed = 0.25
numStep = speed
isTurn = False
isCenterCell = False
count = len(snake)
gameOn = True
userText = ''
isInput = False

while True:
    key = pg.key.get_pressed()
    for event in pg.event.get():
        if event.type == pg.QUIT or key[pg.K_ESCAPE]:
            pg.quit()
            quit()
            f.close()
        if gameOn == False and event.type == pg.KEYDOWN:
            userText += event.unicode

    if gameOn == False:
        isInput = True
        display.fill((0,0,0))
        text1 = textEndGame.render("Введите ник: ", True, (255, 255, 255))
        display.blit(text1,(0,0))
        text2 = textEndGame.render(userText, True, (255, 255, 255))
        display.blit(text2,(200,0))
        if key[pg.K_BACKSPACE]:
            userText = userText[:-1]
        if key[pg.K_RETURN]:
            file.write(userText[:-1] + " - " + str(count) + "\n")
            userText = ''
            gameOn = True
            snake.clear()
            numStep = speed
            isTurn = False
            isCenterCell = False
            snake = [[10, 10, 0], [10, 11, 0]]
            food = [randint(1, column), randint(1, row)]
            count = len(snake)
        
    if gameOn:
        display.fill((100,0,255))
        for i in range(1, column + 1):
            pg.draw.line(display, (0, 25, 25), (i * stepX, 0), (i * stepX, height))
        for i in range(1, row + 1):
            pg.draw.line(display, (0, 25, 25), (0, i * stepY), (width, i * stepY))
        for i in range(1, len(snake)):
            pg.draw.circle(display, (219, 18, 98), (snake[i][0] * stepX - stepX / 2, snake[i][1] * stepY - stepY / 2), 20)
        pg.draw.circle(display, (255, 0, 0), (snake[0][0] * stepX - stepX / 2, snake[0][1] * stepY - stepY / 2), 20)
        pg.draw.circle(display, (0, 255, 0), (food[0] * stepX - stepX / 2, food[1] * stepY - stepY / 2), 15)

        if isCenterCell and food[0] == snake[0][0] and food[1] == snake[0][1]:
            if snake[len(snake) - 1][2] == 0:
                snake.append([snake[len(snake) - 1][0], snake[len(snake) - 1][1] + 1, snake[len(snake) - 1][2]])
            elif snake[len(snake) - 1][2] == 1:
                snake.append([snake[len(snake) - 1][0] + 1, snake[len(snake) - 1][1], snake[len(snake) - 1][2]])
            elif snake[len(snake) - 1][2] == 2:
                snake.append([snake[len(snake) - 1][0], snake[len(snake) - 1][1] - 1, snake[len(snake) - 1][2]])
            elif snake[len(snake) - 1][2] == 3:
                snake.append([snake[len(snake) - 1][0] - 1, snake[len(snake) - 1][1], snake[len(snake) - 1][2]])
            food = [randint(1, column), randint(1, row)]
            count += 1

        for i in range(1, len(snake)):
            if (snake[i][0] == snake[0][0] and snake[i][1] == snake[0][1]) or snake[0][0] < 1 or snake[0][0] > column or snake[0][1] < 1 or snake[0][1] > row:
                gameOn = False

        for i in range(0, len(snake)):
            if snake[i][2] == 0:
                snake[i][1] -= 1 * speed
            elif snake[i][2] == 1:
                snake[i][0] -= 1 * speed
            elif snake[i][2] == 2:
                snake[i][1] += 1 * speed
            elif snake[i][2] == 3:
                snake[i][0] += 1 * speed

        if numStep != 1:
            numStep += speed
            isCenterCell = False
        else:
            numStep = speed
            isCenterCell = True

        for i in range(len(snake) - 1, 0, -1):
            if isCenterCell:
                snake[i][2] = snake[i - 1][2]

        if snake[0][2] != 2 and key[pg.K_w] and isCenterCell:
            snake[0][2] = 0
        elif snake[0][2] != 3 and key[pg.K_a] and isCenterCell:
            snake[0][2] = 1
        elif snake[0][2] != 0 and key[pg.K_s] and isCenterCell:
            snake[0][2] = 2
        elif snake[0][2] != 1 and key[pg.K_d] and isCenterCell:
            snake[0][2] = 3
    pg.display.update()
    clock.tick(40)